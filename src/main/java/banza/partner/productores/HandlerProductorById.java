package banza.partner.productores;

import java.math.BigDecimal;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.rdsdata.AWSRDSData;
import com.amazonaws.services.rdsdata.AWSRDSDataClientBuilder;
import com.amazonaws.services.rdsdata.model.ExecuteStatementRequest;
import com.amazonaws.services.rdsdata.model.ExecuteStatementResult;

import banza.partner.productores.dto.Params;
import banza.partner.productores.dto.ProductoresDto;

public class HandlerProductorById implements RequestHandler<Params, ProductoresDto> {

	@Override
	public ProductoresDto handleRequest(Params event, Context context) {
		AWSRDSData rdsData = AWSRDSDataClientBuilder.standard()
				.withEndpointConfiguration(
						new AwsClientBuilder.EndpointConfiguration(System.getenv("ENDPOINT"), System.getenv("REGION")))
				.build();
		ExecuteStatementRequest request = new ExecuteStatementRequest().withResourceArn(System.getenv("RESOURCE_ARN"))
				.withSecretArn(System.getenv("SECRET_ARN")).withDatabase(System.getenv("BD"))
				.withSql(String.format(System.getenv("QUERY"), event.getIdProductor()));
		ExecuteStatementResult result = rdsData.executeStatement(request);
		ProductoresDto productor = new ProductoresDto();
		if (!result.getRecords().isEmpty()) {
			productor.setNombreCompleto(result.getRecords().get(0).get(1).getStringValue());
			productor.setComision(new BigDecimal(result.getRecords().get(0).get(2).getLongValue()));
		}
		return productor;
	}
}