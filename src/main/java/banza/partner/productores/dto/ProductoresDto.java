package banza.partner.productores.dto;

import java.math.BigDecimal;

public class ProductoresDto {

	private String nombreCompleto;
	private BigDecimal comision;

	public BigDecimal getComision() {
		return comision;
	}

	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

}