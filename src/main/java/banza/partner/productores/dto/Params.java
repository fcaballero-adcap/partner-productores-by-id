package banza.partner.productores.dto;

public class Params {

	private Long idProductor;

	public Long getIdProductor() {
		return idProductor;
	}

	public void setIdProductor(Long idProductor) {
		this.idProductor = idProductor;
	}

}