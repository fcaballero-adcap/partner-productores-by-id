

# Requerimientos

- [Java 8 runtime environment (SE JRE)](https://www.oracle.com/java/technologies/javase-downloads.html)
- [Maven 3](https://maven.apache.org/docs/history.html)


# Compilación

Sobre el directorio del proyecto ejecutar el siguiente comando maven

    $ mvn install


# Configuración de la función Lambda

Para configurar la función lambda se deben definir las siguientes variables de entorno:

Clave "BD" - Valor "nombre de la base"

Clave "ENDPOINT" - Valor "https://rds-data.us-east-1.amazonaws.com"

Clave "QUERY" - Valor "select * from productores where id=%s"

Clave "REGION" - Valor "us-east-1"

Clave "RESOURCE_ARN" - Valor "resource arn de aurora"

Clave "SECRET_ARN" - Valor "secret arn de aurora"


# Despliegue

Cargar en la función lambda el .jar generado en el directorio ..\partner-productores-by-id\target


# Test

Ejemplo JSON de entrada:

    {
        "idProductor": 61
    }
    
Respuesta:

    {
        "nombreCompleto": "CONTIN, AMELIA"
        "comision": 1
    }
    
En caso de no encontrar el productor devuelve un JSON vació.
